import json
import collections
import itertools

import numpy as np

OPS = [
    np.array([[1, 1, 1], [1, 0, 0], [1, 0, 0]]),
    np.array([[1, 1, 1], [0, 1, 0], [0, 1, 0]]),
    np.array([[1, 1, 1], [0, 0, 1], [0, 0, 1]]),
    np.array([[1, 0, 0], [1, 1, 1], [1, 0, 0]]),
    np.array([[0, 1, 0], [1, 1, 1], [0, 1, 0]]),
    np.array([[0, 0, 1], [1, 1, 1], [0, 0, 1]]),
    np.array([[1, 0, 0], [1, 0, 0], [1, 1, 1]]),
    np.array([[0, 1, 0], [0, 1, 0], [1, 1, 1]]),
    np.array([[0, 0, 1], [0, 0, 1], [1, 1, 1]]),
]

solutions = collections.defaultdict(lambda: (4, 4, 4, 4, 4, 4, 4, 4, 4))

for coeffs in itertools.product(*[range(4)] * len(OPS)):
    result = sum(m * c for c, m in zip(coeffs, OPS)) % 4
    k = tuple(result.reshape(9))
    solutions[k] = min(solutions[k], coeffs, key=sum)

with open("data.json", "w") as fd:
    data = {"".join(str(i) for i in k): list(v) for k, v in solutions.items()}
    json.dump(data, fd)
